/*
  SPDX-FileCopyrightText: 2021-2022 Laurent Montel <montel@kde.org>

  SPDX-License-Identifier: GPL-2.0-or-later
*/

#define RUQOLA_VERSION "${RUQOLA_VERSION_STRING}"

#cmakedefine01 HAVE_KUSERFEEDBACK
#cmakedefine01 HAVE_SOLID
#cmakedefine01 HAVE_NETWORKMANAGER

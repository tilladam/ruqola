/*
   SPDX-FileCopyrightText: 2018-2022 Laurent Montel <montel@kde.org>

   SPDX-License-Identifier: LGPL-2.0-or-later
*/

#pragma once

#include "restapiabstractjob.h"

#include "librocketchatrestapi-qt5_export.h"

#include <QNetworkRequest>
namespace RocketChatRestApi
{
class LIBROCKETCHATRESTAPI_QT5_EXPORT PrivateInfoJob : public RestApiAbstractJob
{
    Q_OBJECT
public:
    explicit PrivateInfoJob(QObject *parent = nullptr);
    ~PrivateInfoJob() override;

    Q_REQUIRED_RESULT bool start() override;
    Q_REQUIRED_RESULT bool requireHttpAuthentication() const override;
    Q_REQUIRED_RESULT QNetworkRequest request() const override;
Q_SIGNALS:
    void privateInfoDone(const QJsonObject &data);

private:
    Q_DISABLE_COPY(PrivateInfoJob)
    void onGetRequestResponse(const QJsonDocument &replyJson) override;
};
}

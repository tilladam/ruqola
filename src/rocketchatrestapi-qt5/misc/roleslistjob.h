/*
   SPDX-FileCopyrightText: 2021-2022 Laurent Montel <montel@kde.org>

   SPDX-License-Identifier: LGPL-2.0-or-later
*/

#pragma once

#include "librocketchatrestapi-qt5_export.h"
#include "restapiabstractjob.h"

#include <QNetworkRequest>
namespace RocketChatRestApi
{
class LIBROCKETCHATRESTAPI_QT5_EXPORT RolesListJob : public RestApiAbstractJob
{
    Q_OBJECT
public:
    explicit RolesListJob(QObject *parent = nullptr);
    ~RolesListJob() override;

    Q_REQUIRED_RESULT bool requireHttpAuthentication() const override;

    Q_REQUIRED_RESULT bool start() override;

    Q_REQUIRED_RESULT QNetworkRequest request() const override;

Q_SIGNALS:
    void rolesListDone(const QJsonObject &obj);

private:
    Q_DISABLE_COPY(RolesListJob)
    void onGetRequestResponse(const QJsonDocument &replyJson) override;
};
}

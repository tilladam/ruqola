/*
   SPDX-FileCopyrightText: 2021-2022 Laurent Montel <montel@kde.org>

   SPDX-License-Identifier: LGPL-2.0-or-later
*/

#pragma once

#include "librocketchatrestapi-qt5_export.h"
#include "restapiabstractjob.h"
namespace RocketChatRestApi
{
class LIBROCKETCHATRESTAPI_QT5_EXPORT GroupsConvertToTeamJob : public RestApiAbstractJob
{
    Q_OBJECT
public:
    explicit GroupsConvertToTeamJob(QObject *parent = nullptr);
    ~GroupsConvertToTeamJob() override;

    Q_REQUIRED_RESULT bool start() override;
    Q_REQUIRED_RESULT bool requireHttpAuthentication() const override;
    Q_REQUIRED_RESULT bool canStart() const override;
    Q_REQUIRED_RESULT QNetworkRequest request() const override;

    Q_REQUIRED_RESULT QJsonDocument json() const;

    Q_REQUIRED_RESULT QString roomId() const;
    void setRoomId(const QString &roomId);

Q_SIGNALS:
    void groupConvertToTeamDone(const QJsonObject &obj);

private:
    Q_DISABLE_COPY(GroupsConvertToTeamJob)
    void onPostRequestResponse(const QJsonDocument &replyJson) override;
    QString mRoomId;
};
}

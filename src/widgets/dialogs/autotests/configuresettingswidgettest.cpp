/*
   SPDX-FileCopyrightText: 2020 Laurent Montel <montel@kde.org>

   SPDX-License-Identifier: LGPL-2.0-or-later
*/

#include "configuresettingswidgettest.h"
#include "dialogs/configuresettingswidget.h"
#include <QTest>
QTEST_MAIN(ConfigureSettingsWidgetTest)

ConfigureSettingsWidgetTest::ConfigureSettingsWidgetTest(QObject *parent)
    : QObject(parent)
{
}

/*
   SPDX-FileCopyrightText: 2020-2022 Laurent Montel <montel@kde.org>

   SPDX-License-Identifier: LGPL-2.0-or-later
*/

#include "plugintext.h"

PluginText::PluginText(QObject *parent)
    : QObject(parent)
{
}

PluginText::~PluginText() = default;

/*
   SPDX-FileCopyrightText: 2020-2022 Laurent Montel <montel@kde.org>

   SPDX-License-Identifier: LGPL-2.0-or-later
*/

#include "channelactionpopupmenutest.h"
#include "room/channelactionpopupmenu.h"

#include <QTest>
QTEST_MAIN(ChannelActionPopupMenuTest)

ChannelActionPopupMenuTest::ChannelActionPopupMenuTest(QObject *parent)
    : QObject(parent)
{
}

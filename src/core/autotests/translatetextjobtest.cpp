/*
   SPDX-FileCopyrightText: 2019-2022 Laurent Montel <montel@kde.org>

   SPDX-License-Identifier: LGPL-2.0-or-later
*/

#include "translatetextjobtest.h"
#include <QTest>

QTEST_GUILESS_MAIN(TranslateTextJobTest)

TranslateTextJobTest::TranslateTextJobTest(QObject *parent)
    : QObject(parent)
{
}

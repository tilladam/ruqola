# SPDX-FileCopyrightText: 2020-2022 Laurent Montel <montel@kde.org>
# SPDX-License-Identifier: BSD-3-Clause
add_subdirectory(expandjson)
add_subdirectory(loadroomcache)
add_subdirectory(unicodeemoticongui)
add_subdirectory(uploadfilewidgetlist)
add_subdirectory(notificationhistorygui)
